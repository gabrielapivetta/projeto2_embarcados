#ifndef FLOORS_H
#define FLOORS_H

// GROUND FLOOR
#define LS_T01    4   // Lâmpada da Sala T01                              - Saída
#define LS_T02    17  // Lâmpada da Sala T02                              - Saída
#define LC_T      27  // Lâmpadas do Corredor Terreo                      - Saída
#define AC_T      7   // Ar-Condicionado Terreo                           - Saída
#define SP_T      26  // Sensor de Presença                               - Entrada
#define SF_T      23  // Sensor de Fumaça                                 - Entrada
#define SJ_T01    9   // Sensor de Janela T01                             - Entrada
#define SJ_T02    11  // Sensor de Janela T02                             - Entrada
#define SPo_T     10  // Sensor de Porta Entrada                          - Entrada
#define SC_IN     13  // Sensor de Contagem de Pessoas Entrando no Prédio - Entrada
#define SC_OUT    19  // Sensor de Contagem de Pessoas Saindo do Prédio   - Entrada
#define ASP       16  // Aspersores de Água (Incêndio)                    - Saída

int sensors_output_ground[5] = {LS_T01, LS_T02, LC_T, AC_T, ASP};
int sensors_input_ground[7] = {SP_T, SF_T, SJ_T01, SJ_T02, SPo_T, SC_IN, SC_OUT};


// FIRST FLOOR
#define LS_101    22  // Lâmpada da Sala 101                              - Saída
#define LS_102    25  // Lâmpada da Sala 102                              - Saída
#define LC_1      8   // Lâmpadas do Corredor 1º Andar                    - Saída
#define AC_1      12  // Ar-Condicionado (1º Andar)                       - Saída
#define SP_1      18  // Sensor de Presença                               - Entrada
#define SF_1      24  // Sensor de Fumaça                                 - Entrada
#define SJ101_1   5   // Sensor de Janela 101                             - Entrada
#define SJ102_1   6   // Sensor de Janela 102                             - Entrada

int sensors_output_1_floor[4] = {LS_101, LS_102, LC_1, AC_1};
int sensors_input_1_floor[4] = {SP_1, SF_1, SJ101_1, SJ102_1};

#endif 