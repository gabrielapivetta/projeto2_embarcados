#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "floors.h"

#define SENSOR 0
#define TEMPERATURE 1
#define HUMIDITY 2

// Sockets
int distServerSocket;
int clientSocket;
// Addresses
struct sockaddr_in distServerAddr;
struct sockaddr_in centralServerAddr;
struct sockaddr_in clientAddr;

void message_handler(int, int, int);
void send_message(int, int, float);
void client_handler(int);
void start_listening();
void start_server();
void connect_to_central();
void setup_server();
void shut_down_server();

#endif
