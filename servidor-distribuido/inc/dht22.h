#ifndef DHT22_H
#define DHT22_H

#include <wiringPi.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "server.h"

// CONSTANTS 
#define MAX_TIMINGS 85
#define DEBUG 0
#define WAIT_TIME 2000

// GLOBAL VARIABLES
uint8_t dht_pin_ground = 20;
uint8_t dht_pin_1st = 22; 
uint8_t dht_pin;
char mode = 'c';      // valid modes are c, f, h

int data[5] = {0};
float temp_celsius = -1;
float temp_fahrenheit = -1;
float humidity = -1;

// FUNCTION DEFINITIONSpinMo  
int read_dht_data(float *, float *);
void get_dht_values(int, float *, float *);
void send_dht_values(int);
void printUsage();
int init(int);

#endif