#ifndef GPIO_H
#define GPIO_H

#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <signal.h>
#include "floors.h"
#include "dht22.h"

void turn_on(int);
void turn_off(int);
void setup_gpio();

// ground
void presence_sensor_ground();
void smoke_sensor_ground();
void window_sensor1_ground();
void window_sensor2_ground();
void door_sensor_ground();
void ppl_count_entry_ground();
void ppl_count_exit_ground();

// 1st floor
void presence_sensor_1st();
void smoke_sensor_1st();
void window_sensor1_1st();
void window_sensor2_1st();

void event_handler_ground();
void event_handler_1st();

#endif 
