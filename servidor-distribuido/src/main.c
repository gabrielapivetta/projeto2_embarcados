#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include "server.h"
#include "gpio.h"

void shut_down() {
    shut_down_server();
    exit(0);
}

int main() {
    int port_ground = 10112;
    int port_1st = 10212;

    pthread_t server_ground, server_1st, event_ground, event_1st;
    signal(SIGINT, shut_down);
    setup_gpio();

    pthread_create(&server_ground, NULL, (void*)start_server, &port_ground);
    pthread_create(&server_1st, NULL, (void*)start_server, &port_1st);
    pthread_create(&event_ground, NULL, (void*)event_handler_ground, NULL);
    pthread_create(&event_1st, NULL, (void*)event_handler_1st, NULL);

    while(1) {sleep(1);}
    return 0;
}
