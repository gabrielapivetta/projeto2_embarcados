#include "gpio.h"

static volatile int state_ground[7];
static volatile int state_1st[4];

void turn_on(int pin) {
    digitalWrite(pin, HIGH);
    delay(10);
}

void turn_off(int pin) {
    digitalWrite(pin, LOW);
    delay(10);
}

void setup_gpio() {
    if (wiringPiSetup() == -1) 
        exit(1);
    // ground
    for (int i = 0; i < 5; i++) {
        pinMode(sensors_output_ground[i], OUTPUT);
    }
    for (int i = 0; i < 7; i++) {
        pinMode(sensors_input_ground[i], OUTPUT);
    }
    // 1st floor
    for (int i = 0; i < 4; i++) {
        pinMode(sensors_output_1_floor[i], OUTPUT);
    }
    for (int i = 0; i < 4; i++) {
        pinMode(sensors_input_1_floor[i], OUTPUT);
    }

    // ground
    wiringPiISR(sensors_output_ground[0], INT_EDGE_BOTH, &presence_sensor_ground);
    wiringPiISR(sensors_output_ground[1], INT_EDGE_BOTH, &smoke_sensor_ground);
    wiringPiISR(sensors_output_ground[2], INT_EDGE_BOTH, &window_sensor1_ground);
    wiringPiISR(sensors_output_ground[3], INT_EDGE_BOTH, &window_sensor2_ground);
    wiringPiISR(sensors_output_ground[4], INT_EDGE_BOTH, &door_sensor_ground);
    wiringPiISR(sensors_output_ground[5], INT_EDGE_BOTH, &ppl_count_entry_ground);
    wiringPiISR(sensors_output_ground[6], INT_EDGE_BOTH, &ppl_count_exit_ground);
    // 1st floor
    wiringPiISR(sensors_output_1_floor[0], INT_EDGE_BOTH, &presence_sensor_1st);
    wiringPiISR(sensors_output_1_floor[1], INT_EDGE_BOTH, &smoke_sensor_1st);
    wiringPiISR(sensors_output_1_floor[3], INT_EDGE_BOTH, &window_sensor1_1st);
    wiringPiISR(sensors_output_1_floor[4], INT_EDGE_BOTH, &window_sensor2_1st);
}

// ground
void presence_sensor_ground() {printf("wiringPi 0G: %d\n", digitalRead(sensors_input_ground[0]));}   //{send_message(0, SENSOR, 0);}
void smoke_sensor_ground()    {printf("wiringPi 1G: %d\n", digitalRead(sensors_input_ground[1]));}   //{send_message(0, SENSOR, 1);}
void window_sensor1_ground()  {printf("wiringPi 2G: %d\n", digitalRead(sensors_input_ground[2]));}   //{send_message(0, SENSOR, 2);}
void window_sensor2_ground()  {printf("wiringPi 3G: %d\n", digitalRead(sensors_input_ground[3]));}   //{send_message(0, SENSOR, 3);}
void door_sensor_ground()     {printf("wiringPi 4G: %d\n", digitalRead(sensors_input_ground[4]));}   //{send_message(0, SENSOR, 4);}
void ppl_count_entry_ground() {printf("wiringPi 5G: %d\n", digitalRead(sensors_input_ground[5]));}   //{send_message(0, SENSOR, 5);}
void ppl_count_exit_ground()  {printf("wiringPi 6G: %d\n", digitalRead(sensors_input_ground[6]));}   //{send_message(0, SENSOR, 6);}
// 1st floor  
void presence_sensor_1st()    {printf("wiringPi 0-1: %d\n", digitalRead(sensors_input_1_floor[0]));}  //{send_message(1, SENSOR, 0);}
void smoke_sensor_1st()       {printf("wiringPi 1-1: %d\n", digitalRead(sensors_input_1_floor[1]));}  //{send_message(1, SENSOR, 1);}
void window_sensor1_1st()     {printf("wiringPi 2-1: %d\n", digitalRead(sensors_input_1_floor[2]));}  //{send_message(1, SENSOR, 2);}
void window_sensor2_1st()     {printf("wiringPi 3-1: %d\n", digitalRead(sensors_input_1_floor[3]));}  //{send_message(1, SENSOR, 3);}


void event_handler_ground() {
    signal(SIGALRM, send_dht_values);
    while(1){
        for (int i = 0; i < 7; i++) {
            state_ground[i] = digitalRead(sensors_input_ground[i]);  
            printf("Sensor ground %d: %d\n",i, state_ground[i]);  
            send_dht_values(0);
            if (state_ground[i])
                send_message(0, SENSOR, i);
        }
        sleep(1);
    }
    // while(1) sleep(1);
}

void event_handler_1st() {
    signal(SIGALRM, send_dht_values);
    while(1){
        for (int i = 0; i < 4; i++) {
            state_1st[i] = digitalRead(sensors_input_1_floor[i]);   
            printf("Sensor 1st %d: %d\n",i, state_1st[i]);   
            send_dht_values(1); 
            if (state_1st[i])
                send_message(0, SENSOR, i);
        }sleep(1);
    }
    // while(1) sleep(1);
}