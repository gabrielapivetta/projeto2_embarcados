#include "server.h"

void message_handler(int floor, int pin, int state) {
    if(floor < 1)
		pin = sensors_output_ground[pin-1];
	else
		pin = sensors_output_1_floor[pin-1];

    if (state == 1)
        turn_on(pin);
    else
        turn_off(pin);
}

void send_message(int floor, int type, float obj) {
    char message[7];
    connect_to_central();

    message[0] = floor + '0';
    message[1] = type + '0';
    memcpy(&message[2], &obj, 4);
    message[6] = '\0';

    if(send(clientSocket, message, 7, 0) != 7)
        printf("Erro no envio: numero de bytes enviados diferente do esperado\n");

    close(clientSocket);
}

void client_handler(int clientSocket) {
	char buffer[3];
	int rxLength;
    int floor, pin, state;

	if((rxLength = recv(clientSocket, buffer, 4, 0)) < 0)
		printf("Erro no recv()\n");

	floor = buffer[0] - '0';
	pin = buffer[1] - '0';
	state = buffer[2] - '0';
	message_handler(floor, pin, state);

	while (rxLength > 0) {
		if(send(clientSocket, buffer, rxLength, 0) != rxLength)
			printf("Erro no envio\n");
		
        if((rxLength = recv(clientSocket, buffer, 4, 0)) < 0)
			printf("Erro no recv()\n");
	}
}

void start_listening() {
    unsigned int clientLength;
	while(1) {
		clientLength = sizeof(clientAddr);
		if((clientSocket = accept(distServerSocket, (struct sockaddr *) &clientAddr, &clientLength)) < 0)
			printf("Falha no Accept\n");
		client_handler(clientSocket);
		close(clientSocket);
	}
}

void start_server(void * port) {
    setup_server(port);
    start_listening();
}

void connect_to_central() {
	if((clientSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("Falha no socker do Servidor\n");

    memset(&centralServerAddr, 0, sizeof(centralServerAddr));
	centralServerAddr.sin_family = AF_INET;
	centralServerAddr.sin_addr.s_addr = inet_addr("192.168.0.53");
	centralServerAddr.sin_port = htons(10012);
	if(connect(clientSocket, (struct sockaddr *) &centralServerAddr, sizeof(centralServerAddr)) < 0)
		printf("Erro no connect()\n");
}

void setup_server(void * port) {
	if((distServerSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("Falha no socker do Servidor\n");
	int *porta = (int*)port;
	memset(&distServerAddr, 0, sizeof(distServerAddr));
	distServerAddr.sin_family = AF_INET;
	distServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	distServerAddr.sin_port = htons(*porta);

	if(bind(distServerSocket, (struct sockaddr *) &distServerAddr, sizeof(distServerAddr)) < 0)
		printf("Falha no Bind\n");

	if(listen(distServerSocket, 10) < 0)
		printf("Falha no Listen\n");		
}

void shut_down_server() {
    close(clientSocket);
    close(distServerSocket);
}
