# Projeto 2 - 2021/1

## Servidor Central

### Compilação

Digite o comando abaixo dentro do diretório do projeto:

`$ make`

### Execução

Digite o comando abaixo:

`$ make run` 

### Utilização

Ao executar o programa, a tela será atualizada a cada **1 segundo** com os estados dos dispositivos e sensores, assim como a temperatura e umidade do ambiente (caso o servidor distribuído esteja rodando).

No menu é possível escolher Ligar (L) e Desligar (D) os dispositivos listados. Caso demore mais de 1 segundo para enviar um comando, a entrada da ação (L ou D) pode ser removida da tela.


## Servidor Distribuído

### Compilação
Digite o comando abaixo dentro do diretório do projeto:

`$ make`

### Execução

**Atenção:** somente após executar o servidor central.

Digite o comando abaixo:

`$ make run` 

