#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include "server.h"
#define clear() printf("\033[H\033[J")

void shut_down()
{
  shut_down_server();
  exit(0);
}
void interface()
{
  alarm(1);
  clear();
  printf("Sensores Térreo: 0 [%d] - 1 [%d] - 2 [%d] - 3 [%d] - 4 [%d] - 5 [%d] - 6[%d] - 7 [%d]\n",
         disp_ground[4], disp_ground[5], disp_ground[6], disp_ground[7], disp_ground[8], disp_ground[9], disp_ground[10]);
  printf("Sensores Primeiro Andar: 0 [%d] - 1 [%d] - 2 [%d] - 3 [%d] - 4 [%d]\n",
         disp_1st[4], disp_1st[5], disp_1st[6], disp_1st[7]);
  printf("Temperatura: %.2f - Umidade: %.2f\n\n", temperature, humidity);

  printf("Digite:\nL - Ligar um dispositivo\nD - Desligar um dispositivo\n\n");
  printf("Escolha o dispositivo:\n");
  printf("0. Alarme [%d]\n", alrm);
  printf("Térreo:\n");
  printf("1 - Lampada Sala T01 (Térreo)[%d]\n", disp_ground[0]);
  printf("2 - Lampada Sala T02 (Térreo)[%d]\n", disp_ground[1]);
  printf("3 - Lampadas do corredor (Térreo) [%d]\n", disp_ground[2]);
  printf("4 - Ar-Condicionado (Terreo) [%d]\n", disp_ground[3]);
  printf("5 - Aspersores de Água (Incêndio) (Térreo) [%d]\n", disp_ground[11]);
  printf("Primeiro Andar:\n");
  printf("6 - Lampada Sala T01 (Primeiro Andar)[%d]\n", disp_1st[0]);
  printf("7 - Lampada Sala T02 (Primeiro Andar)[%d]\n", disp_1st[1]);
  printf("8 - Lampadas do corredor (Primeiro Andar) [%d]\n", disp_1st[2]);
  printf("9 - Ar-Condicionado (Primeiro Andar) [%d]\n", disp_1st[3]);
}

void disp_controll(int on_off)
{
  int dispositivo;
  scanf(" %d", &dispositivo);
      if (dispositivo == 0)
        alrm = 1;
      else if (dispositivo <= 5)
        send_message(0, dispositivo, on_off);
      else
        send_message(1, dispositivo, on_off);
}

int main()
{
  pthread_t server;
  char command;

  signal(SIGINT, shut_down);
  signal(SIGALRM, interface);
  init_logger();
  pthread_create(&server, NULL, (void *)start_server, NULL);
  interface();

  while (1)
  {
    scanf("%c", &command);
    switch (command){
    case 'L':
      disp_controll(0);
    case 'D':
      disp_controll(1);
    }
  }

  return 0;
}
